#ifndef __MESSAGE_HPP__
#define __MESSAGE_HPP__

#include <chrono>
#include <algorithm>
#include "Receiver.hpp"

namespace message
{
    template<typename MSG_T>
    class Receiver;
    
    enum class Event
    {
        UPDATE,
        INIT,
        CAN_DATA
    };
    
    template<typename MSG_T>
    class Message
    {
    private:
        void* operator new(size_t size, ReceiverQueue& queue)
        {
            return(static_cast<Message<MSG_T>*>(queue.alloc(size)));
        }
        void* operator new(size_t size, ReceiverQueue& queue, std::chrono::milliseconds delay)
        {
            return(static_cast<Message<MSG_T>*>(queue.alloc(size, delay)));
        }
        
        Message(Receiver<MSG_T>& r, Event e, MSG_T&& d) : event(e), receiver(r), data(d)
        {
        }
        
        Message() = delete;
        Event event;
        Receiver<MSG_T>& receiver;
    public:
        static void send(ReceiverQueue& q, Receiver<MSG_T>& r, Event e, MSG_T&& data, std::chrono::milliseconds delay)
        {
            if (q.enoughTimeoutSpace(sizeof(Message<MSG_T>)))
            {
                new(q, delay) Message<MSG_T>(r, e, std::move(data));
            }
        }
        static void send(ReceiverQueue& q,Receiver<MSG_T>& r, Event e, MSG_T&& data)
        {
            if (q.enoughSpace(sizeof(Message<MSG_T>)))
            {
                new(q) Message<MSG_T>(r, e, std::move(data));
            }
        }
        void deliver()
        {
            receiver.receive(*this);
        }
        MSG_T data;
    };
}

#endif //__MESSAGE_HPP__
